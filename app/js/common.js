$(function () {
    $(document).ready(function () {
        $(document).on('click', '.nav_label', function(){
            $('.nav').toggleClass('visible');
        });

        $(document).on('click', '.nav ul li span', function(e){

            if($(this).parent().hasClass('open')){
                $(this).siblings('ul').css('display', 'block');
                $(this).siblings('ul').slideUp( '350', function(){}); 
            } else{
                $(this).siblings('ul').slideDown( '350', function(){}); 
            }

            $(this).parent().toggleClass('open');
            
        });
    });
});